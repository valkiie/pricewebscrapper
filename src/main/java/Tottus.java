import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;

public class Tottus {
    public static void main(String[] args) throws IOException {
        String url= "https://www.tottus.com.pe/laive-leche-sin-lactosa-40983924/p/";
        Document doc = Jsoup.connect(url).get();
        System.out.println(doc.title());
        Elements newsHeadlines = doc.select("#mp-itn b a");
        for (Element headline : newsHeadlines) {
            System.out.println("%s\n\t%s"+
                    headline.attr("title") +"\n"+ headline.absUrl("href"));
        }


        System.out.printf("\nWebsite Title: %s\n\n", doc.title());

        // Get the list of repositories
        Elements elements = doc.getAllElements();
        Elements pricing = doc.select(".ProductPrice");
        /*for (Element element : elements){
            System.out.println(element.text());
        }*/
        for (Element element : pricing){
            System.out.println("Price:"+element.text());
        }
    }
}
